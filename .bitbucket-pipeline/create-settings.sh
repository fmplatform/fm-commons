#!/usr/bin/env bash

cp .bitbucket-pipeline/settings.template.xml settings.xml

sed -i -e 's/%MVN_REPO_USERNAME%/'"$MVN_REPO_USERNAME"'/g' settings.xml
sed -i -e 's/%MVN_REPO_PASSWORD%/'"$MVN_REPO_PASSWORD"'/g' settings.xml