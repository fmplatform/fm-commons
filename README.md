# FM Commons
This project is a maven library that contains multiple utility classes and including dto/models or configuration 
that are otherwise used across multiple microservices

## Devs Requirements

For development purposes you need:

- [JDK 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Maven 3](https://maven.apache.org)

## Architecture
- `com.fm.commons` : base package of project.
    ### Sub Packages
    - `constants` : contains class that basically house constant variables.
    - `domain` : contains utility classes concerned with date manipulation and tenant management.
    - `dto` : contains `Data Transfer Object`s used by various service for response transformation.
    - `enums` : keeps enumerator classes.
    - `exceptions` : contains custom exception classes.
    - `error` : contains a class for transforming error responses and exceptions.
    - `interfaces` : cointains interface class for various purposes.
    - `utils` : cointains utility classes.
- `resources` : contains config files.
## How to use it
Add server details and your repo user details in `setting.xml`(`user.home/.m2/settings.xml`) 
``` shell
   <servers>
      <server>
            <id>helixteamhub.cloud</id>
            <username>your_username</username>
            <password>your_password</password>
        </server>
   </servers>
```
Add below `repository` in your `pom.xml` file

``` shell
 <repository>
    <id>helixteamhub.cloud</id>
    <url>https://helixteamhub.cloud/financemobile/projects/fm/repositories/maven/fm-commons</url>
 </repository>
```
After add the `fm-commons` library as a `dependency` in you `pom.xml`, as show below:


``` shell
   <dependency>
     <groupId>com.fm</groupId>
     <artifactId>fm-commons</artifactId>
     <version>0.0.2-SNAPSHOT</version>
   </dependency>
```

