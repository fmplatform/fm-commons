package com.fm.commons.errors;

import lombok.Data;

@Data
public class BaseError {

    private String url;
    private int errorCode;
    private String errorMessage;

}
