package com.fm.commons.exceptions;


import com.fm.commons.enums.ResponseMessage;

public class AuthException extends RuntimeException {

    private int code;
    private String message;

    public AuthException(int code, String message){
        this.code = code;
        this.message = message;
    }


    public AuthException(ResponseMessage responseMessage){
        this.code = responseMessage.getCode();
        this.message = responseMessage.getMessage();
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
