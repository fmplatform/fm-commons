package com.fm.commons.exceptions;

import com.fm.commons.enums.ResponseMessage;

public class DataNotFoundException extends RuntimeException {

    private int code;
    private String message;

    public DataNotFoundException() {
    }

    public DataNotFoundException(int code, String message){
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataNotFoundException(ResponseMessage response){
        this.code = response.getCode();
        this.message = response.getMessage();
    }

    public DataNotFoundException(String message) {
        super(message);
    }
}
