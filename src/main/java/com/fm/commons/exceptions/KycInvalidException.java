package com.fm.commons.exceptions;

import com.fm.commons.enums.ResponseMessage;

public class KycInvalidException extends RuntimeException{

    private int code;
    private String message;

    public KycInvalidException() {
    }

    public KycInvalidException(int code, String message){
        this.code = code;
        this.message = message;
    }

    public KycInvalidException(ResponseMessage response){
        this.code = response.getCode();
        this.message = response.getMessage();
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }


}
