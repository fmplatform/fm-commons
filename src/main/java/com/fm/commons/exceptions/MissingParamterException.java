package com.fm.commons.exceptions;

public class MissingParamterException extends Exception {

    public MissingParamterException() {
    }

    public MissingParamterException(String message) {
        super(message);
    }

    public MissingParamterException(String message, Throwable cause) {
        super(message, cause);
    }
}
