package com.fm.commons.exceptions;

public class AlreadyExistException extends Exception {

    private int code;
    private String message;

    public AlreadyExistException(int code, String message){
        this.code = code;
        this.message = message;
    }

    public AlreadyExistException(String message){
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
