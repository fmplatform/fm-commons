package com.fm.commons.exceptions;

public class UnauthorizedException extends RuntimeException {

    private String principal;

    public UnauthorizedException(String principal){
        this.principal = principal;
    }

    public String getPrincipal() {
        return principal;
    }

    public void setPrincipal(String principal) {
        this.principal = principal;
    }

    @Override
    public String getMessage() {
        return "User Unauthorized";
    }

}