package com.fm.commons.exceptions;

import com.fm.commons.enums.ResponseMessage;

public class AccountInActiveException extends RuntimeException {

    private int code;
    private String message;

    public AccountInActiveException(String s) {
    }

    public AccountInActiveException(int code, String message){
        this.code = code;
        this.message = message;
    }

    public AccountInActiveException(ResponseMessage response){
        this.code = response.getCode();
        this.message = response.getMessage();
    }
}
