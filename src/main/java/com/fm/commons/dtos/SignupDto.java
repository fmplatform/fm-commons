package com.fm.commons.dtos;


import com.fm.commons.enums.ExternalAccountType;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class SignupDto implements Serializable {

    @NotNull
    private ExternalAccountType type;
    private String account;
    private String countryCode;
    private String firstName;
    private String lastName;
    @NotNull
    private String phone;
    private String email;

}