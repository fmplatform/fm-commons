package com.fm.commons.dtos;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;


@Data
public class PrivacyPolicyDto implements Serializable {

    @NotNull
    @NotEmpty
    private String value;
    @NotNull
    @NotEmpty
    private String francophoneValue;
    private int version;
    private String updatedBy;
    private String createdAt;
    private String updatedAt;
}