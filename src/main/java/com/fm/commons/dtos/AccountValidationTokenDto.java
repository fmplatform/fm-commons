package com.fm.commons.dtos;

import lombok.Data;

@Data
public class AccountValidationTokenDto {

    private String token;
    private String userId;
    private long expiresAt;
    private String createdAt;
    private String updatedAt;

}