package com.fm.commons.dtos;

import lombok.Data;

@Data
public class OtpDto {

    private String prefix;
    private String value;
    private String phone;
    private String phoneKey;
    private String maskedPhone;
    private String email;
    private String status;
    private String purpose;

}