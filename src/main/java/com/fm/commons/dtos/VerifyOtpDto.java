package com.fm.commons.dtos;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class VerifyOtpDto {

    @NotEmpty
    private String otp;
    private String key;
    private String email;
    private String phone;

}
