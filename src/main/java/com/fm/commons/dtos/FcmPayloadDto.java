package com.fm.commons.dtos;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

@Data
public class FcmPayloadDto implements Serializable {

    @NotEmpty
    @NotBlank
    @Size(max = 150, min = 2)
    private String pushType;
    private String topic;
    @NotEmpty
    @NotBlank
    @Size(max = 150, min = 2)
    private String title;
    @NotEmpty
    @NotBlank
    @Size(max = 250, min = 2)
    private String description;
    private ActivityDto activity;
    private String data;
    @NotEmpty
    private List<String> usersPhoneNumbers;

    @Override
    public String toString() {

        Gson gson = new GsonBuilder()
                .serializeNulls()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();

        return gson.toJson(this, FcmPayloadDto.class);
    }
}