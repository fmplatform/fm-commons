package com.fm.commons.dtos;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ResetPassCodeDto extends SetPassCodeDto {

    @NotNull
    private String otp;

}
