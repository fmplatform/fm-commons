package com.fm.commons.dtos;

import com.fm.commons.enums.OtpPurpose;
import lombok.Data;

@Data
public class SendOtpDto {

    private String email;
    private String phone;
    private String phoneKey;
    private String maskedPhone;
    private OtpPurpose otpPurpose;

}
