package com.fm.commons.dtos;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class SetOnboardingStatusDto {
    @NotEmpty
    private String userPhone;
    @NotEmpty
    private String onboardingStatus;

}
