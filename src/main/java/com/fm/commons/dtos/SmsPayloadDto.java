package com.fm.commons.dtos;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class SmsPayloadDto implements Serializable {

    private String title;
    private String message;
    private String from;
    private List<String> toPhoneNumbers;
}