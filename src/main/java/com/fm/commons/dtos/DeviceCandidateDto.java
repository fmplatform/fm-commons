package com.fm.commons.dtos;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.Data;

@Data
public class DeviceCandidateDto {

    private String id;
    private String userPhone;
    private String userEmail;
    private String candidateId;
    private long expiresAt;
    private String fcmToken;
    private String instanceId;
    private String model;
    private String brand;
    private String manufacturer;
    private String createdAt;
    private String updatedAt;

    public String toActivityData() {
        Gson gson = new GsonBuilder()
                .serializeNulls()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();

        return gson.toJson(this, DeviceCandidateDto.class);
    }
}