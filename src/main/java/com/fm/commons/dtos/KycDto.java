package com.fm.commons.dtos;

import com.fm.commons.enums.ExternalAccountType;
import lombok.Data;

import java.io.Serializable;

@Data
public class KycDto implements Serializable {

    private String id;
    private String name;
    private String userPhone;
    private String userEmail;
    private String tenantId;
    private String jumioReference;
    private ExternalAccountType type;
    private String account;
    private String firstName;
    private String lastName;

}
