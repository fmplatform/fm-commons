package com.fm.commons.dtos;

import com.fm.commons.enums.ExternalAccountType;
import lombok.Data;

@Data
public class CreateEDCAccountDto {

    private String account;
    private ExternalAccountType type;
    private String fname;
    private String sname;
    private String email;
    private String telephone;
    private String product;
    private String bank_name;
    private String bank_acc;
    private String cnty;
    private String cif;
    private boolean kycValid;
}
