package com.fm.commons.dtos;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class SetPassCodeDto {

    @NotNull
    private String pc;
    @NotNull
    private String phone;

}