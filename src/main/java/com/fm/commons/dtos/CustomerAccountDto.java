package com.fm.commons.dtos;


import com.fm.commons.enums.AccountStatus;
import lombok.Data;

import java.sql.Timestamp;
import java.util.Set;

@Data
public class CustomerAccountDto {

    private String id;

    private String account;

    private String customerId;

    private String accountName;

    private String preferredBankAccount;

    private String preferredBank;

    private AccountStatus status;

    private Timestamp createdAt;

    private Timestamp updatedAt;

    private String tenantId;

    private Set<AccountPortfolioDto> portfolios;

}
