package com.fm.commons.dtos;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.Data;

import java.io.Serializable;

@Data
public class ActivityDto implements Serializable {

    private String id;
    private String userPhone;
    private String action;
    private String pushType;
    private String description;
    private String resourceType;
    private String resourceId;
    private String activityReference;
    private String data;
    private String createdAt;
    private String updatedAt;

    @Override
    public String toString() {

        Gson gson = new GsonBuilder()
                .serializeNulls()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();

        return gson.toJson(this, ActivityDto.class);
    }
}
