package com.fm.commons.dtos;

import com.fm.commons.enums.OtpPurpose;
import com.fm.commons.enums.WalletType;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.Map;

@Data
public class CreateWalletDto {

    @NotNull
    private String userPhone;
    @NotNull
    private WalletType walletType;
    @NotNull
    private String walletProvider;
    @NotNull
    private String accountId;
    private String name;
    private String accountName;
    private String otpValue;
    private String otpInstrument;
    private OtpPurpose otpPurpose;
    private Map<String, Object> extras = new HashMap<>();
}
