package com.fm.commons.dtos;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

@Data
public class CreateUserDto implements Serializable {

    @NotNull
    @Email
    private String email;
    @NotNull
    private String phone;
    @NotNull
    @Size(min = 2, max = 200)
    private String firstName;
    @NotNull
    @Size(min = 2, max = 200)
    private String lastName;
    @Size(min = 2, max = 200)
    private String otherNames;
    @NotNull
    private CountryDto country;
    @NotEmpty
    private List<RoleDto> roles;

}
