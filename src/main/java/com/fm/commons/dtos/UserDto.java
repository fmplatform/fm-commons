package com.fm.commons.dtos;

import lombok.Data;
import org.apache.http.util.TextUtils;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Data
public class UserDto implements Serializable {

    private String id;
    private String email;
    private String tenantId;
    private String phone;
    private String firstName;
    private String lastName;
    private String otherNames;
    private String fullName;
    private String customerId;
    private String onboardingStatus;
    private String originatingSystem;
    private boolean accountClaimed;
    private boolean hasAcceptedTerms;
    private Timestamp lastLogin;
    private String acceptedTermsOn;
    private String lastSeen;
    private String createdAt;
    private String updatedAt;
    private CountryDto country;
    private List<RoleDto> roles = new ArrayList<>();

    public String getFullName() {
        if (TextUtils.isEmpty(fullName)) {
            fullName =  firstName + (TextUtils.isEmpty(otherNames) ? "" : (" " + otherNames)) + " " + lastName;
        }

        return fullName;
    }
}
