package com.fm.commons.dtos;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class CountryDto implements Serializable {

    private String id;
    private String code;
    private String name;
    private String customerIdPrefix;
    private String currencyCode;
    private String currencyName;
    private String createdBy;
    private List<PaymentMethodDto> paymentMethods;

}
