package com.fm.commons.dtos;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class SetPasswordDto {

    @NotNull
    private String password;
    @NotNull
    private String validationToken;

}