package com.fm.commons.dtos;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@Data
public class MutualFundDto implements Serializable {

    private String id;
    private String fundManagerPhone;
    private String name;
    private String fundShortName;
    private boolean published;
    private double endOfDayPrice;
    private double bestReturn;
    private double worstReturn;
    private String dateOfBestReturn;
    private String dateOfWorstReturn;
    private String investmentObjective;
    private String strategy;
    private String performance;
    private String createdBy;
    private String createdAt;
    private String updatedAt;
    private BigDecimal maximumAmount;
    private BigDecimal minimumAmount;
    private BigDecimal initialDeposit;
    private BigDecimal additionalDeposit;
    private Integer minimumHoldingPeriod;
    private Map<String,Object> penaltyRates = new HashMap<>();
    private Map<String, String> topHoldings = new HashMap<>();
    private Map<String, String> returns = new HashMap<>();
    private String lastValuationDate;

}