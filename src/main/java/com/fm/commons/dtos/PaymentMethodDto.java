package com.fm.commons.dtos;

import lombok.Data;

@Data
public class PaymentMethodDto {

    private String name;
    private String id;
    private String countryCode;

}
