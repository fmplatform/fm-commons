package com.fm.commons.dtos;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Data
public class BiLoginDto {

    @NotNull
    @Email
    private String email;
    @NotNull
    private String password;

}