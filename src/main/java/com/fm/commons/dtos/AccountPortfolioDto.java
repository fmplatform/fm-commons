package com.fm.commons.dtos;

import lombok.Data;

import java.math.BigDecimal;
import java.sql.Timestamp;

@Data
public class AccountPortfolioDto {


    private String id;

    private String account;

    private String accountName;

    private String fund;

    private String currency;

    private BigDecimal position;

    private String finalClient;

    private BigDecimal buyingAmount;

    private BigDecimal pnl;

    private BigDecimal quotation;

    private String distributor;

    private Timestamp lastEvaluationDate;

    private Timestamp createdAt;

}
