package com.fm.commons.dtos;

import lombok.Data;

@Data
public class UsersGroupByCountry {

    private String country;
    private Long count;

    public UsersGroupByCountry(String country, Long count) {
        this.country = country;
        this.count = count;
    }
}
