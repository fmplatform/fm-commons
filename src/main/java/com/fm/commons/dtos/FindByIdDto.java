package com.fm.commons.dtos;

import lombok.Data;

@Data
public class FindByIdDto {

    private String id;

}
