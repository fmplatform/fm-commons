package com.fm.commons.dtos;

import lombok.Data;

@Data
public class UsersGroupByTypeDto {

    private String onboardingType;
    private Long count;

    public UsersGroupByTypeDto(String onboardingType, Long count) {
        this.onboardingType = onboardingType;
        this.count = count;
    }
}
