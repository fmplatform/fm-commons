package com.fm.commons.dtos;

import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Data
public class    EmailPayloadDto implements Serializable {

    private String subject;
    private String message;
    private List<String> toEmails;
    private Map<String, String> attachments;

}