package com.fm.commons.dtos;

import lombok.Data;

import java.io.Serializable;

@Data
public class DeviceDto implements Serializable {

    private String id;
    private String userPhone;
    private String fcmToken;
    private String instanceId;
    private String model;
    private String brand;
    private String manufacturer;
    private String createdAt;
    private String updatedAt;

}