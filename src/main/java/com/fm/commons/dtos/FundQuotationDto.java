package com.fm.commons.dtos;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class FundQuotationDto {

    private String fund;
    private BigDecimal quotation;

    public FundQuotationDto(String fund, BigDecimal quotation) {
        this.fund = fund;
        this.quotation = quotation;
    }
}
