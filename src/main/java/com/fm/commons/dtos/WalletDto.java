package com.fm.commons.dtos;

import com.fm.commons.enums.WalletType;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class WalletDto {

    private String id;
    private String userPhone;
    private WalletType walletType;
    private String walletProvider;
    private String accountId;
    private String name;
    private String accountName;
    private Map<String,Object> extras = new HashMap<>();
    private String createdAt;
    private String updatedAt;

    public String toActivityData() {
        Gson gson = new GsonBuilder()
                .serializeNulls()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();

        return gson.toJson(this, WalletDto.class);
    }
}
