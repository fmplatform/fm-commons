package com.fm.commons.dtos;

import lombok.Data;

@Data
public class LoginDto {

    private String value;
    private String phone;

}