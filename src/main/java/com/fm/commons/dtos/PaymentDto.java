package com.fm.commons.dtos;


import com.fm.commons.enums.PaymentState;
import com.fm.commons.enums.TransactionType;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class PaymentDto implements Serializable {

    private String id;
    private String userPhone;
    private String walletId;
    private String resourceId;
    private String clientReference;
    private String description;
    private String method;
    private BigDecimal amount;
    private String currency;
    private PaymentState paymentState;
    private String statusCode;
    private String statusMessage;
    private TransactionType transactionType;
    private String createdAt;
    private String updatedAt;

}