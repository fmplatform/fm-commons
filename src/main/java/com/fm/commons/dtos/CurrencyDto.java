package com.fm.commons.dtos;

import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

@Data
public class CurrencyDto implements Serializable {

    private String id;
    private String code;
    private String name;
    private Timestamp createdAt;
    private Timestamp updatedAt;
    private String createdBy;
    private CountryDto country;

}
