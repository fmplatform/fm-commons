package com.fm.commons.dtos;

import lombok.Data;

@Data
public class ExternalAccountDto {

    private String id;
    private String phone;
    private String account;
    private String type;

}
