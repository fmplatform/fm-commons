package com.fm.commons.dtos;

import lombok.Data;

import java.io.Serializable;

@Data
public class LegalDto implements Serializable {

    private TermsOfServiceDto termsOfService;
    private PrivacyPolicyDto privacyPolicy;

}