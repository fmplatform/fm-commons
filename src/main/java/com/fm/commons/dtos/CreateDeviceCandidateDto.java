package com.fm.commons.dtos;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class CreateDeviceCandidateDto implements Serializable {

    @NotNull
    private String countryCode;
    @NotNull
    private String userPhone;
    @NotNull
    private String fcmToken;
    @NotNull
    private String instanceId;
    @NotNull
    private String model;
    @NotNull
    private String brand;
    @NotNull
    private String manufacturer;

}
