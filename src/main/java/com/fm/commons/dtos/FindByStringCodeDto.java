package com.fm.commons.dtos;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class FindByStringCodeDto {

    @NotEmpty
    private String code;
}
