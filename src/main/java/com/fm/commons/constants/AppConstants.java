package com.fm.commons.constants;

public interface AppConstants {

     static final int OTP_EXPIRATION_MILLIS = 3 * 60 * 1000;  // 180 seconds
     static final int ACCOUNT_VALIDATION_TOKEN_EXPIRATION_MILLIS = 24 * 60 * 60 * 1000;  // 1 day
     static final int PRIMARY_DEVICE_ELECTION_EXPIRATION_MILLIS = 5 * 60 * 1000;  // 5 mins
     static final String SUCCESS_MESSAGE = "success";
     static final String FAILED_MESSAGE = "failed";
     static final String CLIENT_ORIGIN = "FM_SERVICE";
}