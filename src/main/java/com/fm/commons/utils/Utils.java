package com.fm.commons.utils;

import com.fm.commons.exceptions.KycInvalidException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.fm.commons.domain.FmDate;
import com.fm.commons.domain.TenantContext;
import com.fm.commons.enums.ResponseMessage;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.RandomStringUtils;
import org.bouncycastle.util.Strings;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class Utils {

    private static final Logger log = LoggerFactory.getLogger(Utils.class);

    public static File[] getFilesFromDirInExternalStorage(String dirPath) {
        File[] files = null;

        File f = new File(dirPath);
        files = f.listFiles();
        return files;
    }

    public static String convertInputStream2String(InputStream stream) {

        BufferedReader reader = new BufferedReader(
                new InputStreamReader(stream));
        StringBuilder str = new StringBuilder();
        String line = "";

        try {
            while ((line = reader.readLine()) != null) {
                str.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return str.toString();
    }

    public static String getAPIStatus(JSONObject jsonObject) {
        // the default value should never be the same any of the http status
        // codes returned by the API.
        String successValue = "no";
        try {
            successValue = jsonObject.getString("success");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return successValue;
    }

    public static Object getJSONEntity(JSONObject jsonObject, String entity) {
        // the default value should never be the same any of the http status
        // codes returned by the API.
        Object result = null;
        try {
            result = jsonObject.get(entity);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static JSONObject convertStreamToJSONObject(InputStream is)
            throws Exception {
        String json;
        JSONObject jObj;
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            json = sb.toString();
            log.info(">>> INFO Process: " + json);
            // try parse the string to a JSON object
            try {
                jObj = new JSONObject(json);
                // return JSON Obj
                return jObj;
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public static JSONArray convertStreamToJSONArray(InputStream is)
            throws Exception {
        String json;
        JSONArray jsonArray;
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            json = sb.toString();
            log.info(">>> INFO Process: " + json);
            // try parse the string to a JSON object
            try {
                jsonArray = new JSONArray(json);
                return jsonArray;
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public static String convertStreamToString(InputStream is)
            throws Exception {
        String response;
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            response = sb.toString();
            log.info(">>> INFO Error Process: " + response);
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String generateDateTime() {
        DateFormat dateFormatUTC = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return dateFormatUTC.format(new Date());
    }

    public static String getReleaseDateFromCalendar(Calendar calendar) {
        DateFormat dateFormatUTC = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return dateFormatUTC.format(calendar.getTime());
    }

    public static String generateDateFromDaysPassed(int daysPassed) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, -(daysPassed));
        Date date = calendar.getTime();

        DateFormat dateFormatUTC = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return dateFormatUTC.format(date);
    }

    public static String getReadableDate(String barzDateString) {
        Calendar calendar = getCalendarFromDate(barzDateString);
        Date date = calendar.getTime();

        DateFormat dateFormatUTC = new SimpleDateFormat("d MMM, yyyy.");
        return dateFormatUTC.format(date);
    }

    public static String getReadableDateTime(String barzDateString) {
        Calendar calendar = getCalendarFromDate(barzDateString);
        Date date = calendar.getTime();

        DateFormat dateFormatUTC = new SimpleDateFormat("d MMM, yyyy. HH:mm");
        return dateFormatUTC.format(date);
    }

    public static String getReadableDateFull(String barzDateString) {
        Calendar calendar = getCalendarFromDate(barzDateString);
        Date date = calendar.getTime();

        DateFormat dateFormatUTC = new SimpleDateFormat("E d MMM, yyyy.");
        return dateFormatUTC.format(date);
    }

    public static String generateExpiryDate(int daysFromToday) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, (daysFromToday));
        Date date = calendar.getTime();

        DateFormat dateFormatUTC = new SimpleDateFormat("yyyy-MM-dd");
        return dateFormatUTC.format(date);
    }

    public static Calendar getCalendarFromDate(String barzDateString) {
        FmDate bd = new FmDate(barzDateString);

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, Integer.valueOf(bd.getYear()));
        calendar.set(Calendar.MONTH, Integer.valueOf(bd.getMonth()) - 1);
        calendar.set(Calendar.DAY_OF_MONTH, Integer.valueOf(bd.getDay()));
        calendar.set(Calendar.HOUR_OF_DAY, Integer.valueOf(bd.getHour()));
        calendar.set(Calendar.MINUTE, Integer.valueOf(bd.getMinutes()));

        return calendar;
    }

    public static boolean isTimeElapsed(long timeMillis) {
        return System.currentTimeMillis() > timeMillis;
    }

    public static String daysPassedFromDateCreated(String dateCreated) {

        if (isEmptyOrNull(dateCreated))
            return "Date not set";

        String datePart = dateCreated.split("T")[0];

        String[] parts = datePart.split("-");
        if (parts.length >= 3) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.YEAR,
                    Integer.parseInt(parts[0]));
            calendar.set(Calendar.MONTH,
                    Integer.parseInt(parts[1]) - 1);
            calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(parts[2]));

            long difference = Calendar.getInstance().getTime().getTime()
                    - calendar.getTime().getTime();
            long diffDays = difference / (24 * 60 * 60 * 1000);

            return daysPassed(diffDays);
        } else
            return "";
    }

    public static String daysPassed(long daysPassed) {

        String output = "";
        if (daysPassed == 0)
            output += "today";
        else if (daysPassed == 1)
            output += "yesterday";
        else if (daysPassed > 1)
            output += daysPassed + "d";
        return output;
    }

    public static String likes(int likes) {
        String output = "";
        if (likes == 0)
            output += "0 likes";
        else if (likes == 1)
            output += likes + " like";
        else if (likes > 1)
            output += likes + " likes";
        return output;
    }

    public static String likesInDays(int likes, String dateCreated) {
        String datePart = dateCreated.split(" ")[0];
        String[] parts = datePart.split("-");
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR,
                Integer.parseInt(parts[0]));
        calendar.set(Calendar.MONTH,
                Integer.parseInt(parts[1]) - 1);
        calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(parts[2]));

        long difference = Calendar.getInstance().getTime().getTime()
                - calendar.getTime().getTime();
        long diffDays = difference / (24 * 60 * 60 * 1000);

        String output = "";
        if (likes == 0)
            output += likes + " likes";
        else if (likes == 1)
            output += likes + " like";
        else if (likes > 1)
            output += likes + " likes";

        if (diffDays == 0)
            output += " first day";
        else if (diffDays == 1)
            output += " in 1 day";
        else if (diffDays > 1)
            output += " in " + diffDays + " days";

        return output;
    }

    public static String noOfTracks(int no) {
        String output = "";
        if (no == 0)
            output += "no track";
        else if (no == 1)
            output += "1 track";
        else if (no > 1)
            output += no + " tracks";
        return output;
    }

    public static String generateUniqueID() {
        return UUID.randomUUID().toString();
    }


    public static boolean isEmptyOrNull(String input) {
        return input == null || input.length() == 0;
    }

    public static String hashString(String secret) {

        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(secret.getBytes());

            byte byteData[] = md.digest();

            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < byteData.length; i++) {
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16)
                        .substring(1));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getSizeAsString(long size) {
        Double sizeDouble = (double) size;

        String unit = "B";
        if (sizeDouble > 1024) {
            sizeDouble = sizeDouble / 1024;
            unit = "KB";
        }
        if (sizeDouble > 1024) {
            sizeDouble = sizeDouble / 1024;
            unit = "MB";
        }

        return formatSize(sizeDouble) + " " + unit;
    }

    public static Double getSizeInMB(long size) {
        Double sizeDouble = (double) size;
        sizeDouble = sizeDouble / (1024 * 1024);

        return Double.valueOf(formatSize(sizeDouble));
    }

    public static String formatSize(double size) {
        DecimalFormat df = new DecimalFormat("#.00");
        return df.format(size);
    }

    public static String getFileMimeType(String fileName) {

        if (!fileName.isEmpty()) {
            if (fileName.endsWith(".png"))
                return "image/png";
            else if (fileName.endsWith(".jpg") || fileName.endsWith(".jpeg"))
                return "image/jpeg";
        }
        return null;
    }


    public static String generateClientApiKey() {
        return UUID.randomUUID().toString();
    }

    public static String generateTransactionCode() {
        String uniqueID = UUID.randomUUID().toString();
        String[] parts = uniqueID.split("-");
        String joinedParts = String.join("", parts);

        return joinedParts + System.currentTimeMillis();
    }

    public static String generateTransactionId() {
        String uniqueID = UUID.randomUUID().toString();
        String[] parts = uniqueID.split("-");
        String joinedParts = String.join("", parts);

        return joinedParts;
    }

    public static String getClientIp(HttpServletRequest request) {
        String remoteAddr = "";

        if (request != null) {
            remoteAddr = request.getHeader("X-FORWARDED-FOR");
            if (remoteAddr == null || "".equals(remoteAddr)) {
                remoteAddr = request.getRemoteAddr();
            }
        }
        return remoteAddr;
    }

    public static String generateReceiptNumber() {
        //?-Random 4 digit number-dateSequence-timeSequence
        //xxxxx-xxxx-yyMMdd-HHmmss

        int fourDigitNumber = new Random().nextInt(9999);

        Date date = new Date();

        DateFormat dateFormatUTC = new SimpleDateFormat("yyMMdd", Locale.getDefault());
        String dateSequence = dateFormatUTC.format(date);

        dateFormatUTC = new SimpleDateFormat("HHmmss", Locale.getDefault());
        String timeSequence = dateFormatUTC.format(date);

        return /*String.format(Locale.getDefault(), "%05d", businessAId)
                + "-"
                +*/ String.format(Locale.getDefault(), "%04d", fourDigitNumber)
                + "-"
                + String.format(Locale.getDefault(), "%06d", Integer.parseInt(dateSequence))
                + "-"
                + String.format(Locale.getDefault(), "%06d", Integer.parseInt(timeSequence));
    }

    public static String generateOtp() {
        //letters-number
        //xxxx-xxxx
        //eg. WRFU-2343


        String fourDigitAlphabetic = Strings.toUpperCase(RandomStringUtils.randomAlphabetic(4));
        int fourDigitNumber = new Random().nextInt(9999);
        return fourDigitAlphabetic + "-"
                + String.format(Locale.getDefault(), "%04d", fourDigitNumber);
    }

    public static String gererateDefaultPassword() {
        return Strings.toUpperCase(RandomStringUtils.randomAlphanumeric(12));
    }

    public static String internationalizePhoneNumber(String countryCode, String phone) throws Exception {
        phone = stripIntoDigits(phone);
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
        Phonenumber.PhoneNumber phoneNumber = phoneUtil.parse(phone, countryCode);
        PhoneNumberUtil.PhoneNumberType type =  phoneUtil.getNumberType(phoneNumber);

        if (type != PhoneNumberUtil.PhoneNumberType.MOBILE){
            throw new KycInvalidException(ResponseMessage.PHONE_NUMBER_NOT_MOBILE_NUMBER);
        }

        return phoneUtil.format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL).replace(" ", "");
    }

    public static double formatAmountForDb(Double amount) {
        return Double.valueOf(String.format("%,.2f", amount));
    }

    public static String maskPhoneNumber(String phone) throws Exception{
        // cc from https://stackoverflow.com/questions/7480608/masking-a-creditcard-number-in-java

        String tenantId = TenantContext.getCurrentTenant();

        phone = internationalizePhoneNumber(tenantId, phone);

        // format the number
//        int index = 0;
//        String mask = "######****###";
//        StringBuilder maskedNumber = new StringBuilder();
//        for (int i = 0; i < mask.length(); i++) {
//            char c = mask.charAt(i);
//            if (c == '#') {
//                maskedNumber.append(phone.charAt(index));
//                index++;
//            } else if (c == '*') {
//                maskedNumber.append(c);
//                index++;
//            } else {
//                maskedNumber.append(c);
//            }
//        }

        // return the masked number
        return  phone.substring(0, 6) + "****" + phone.substring(phone.length() - 3);
    }

    public static String convertToBase64String(byte[] bytes){
        if(null != bytes)
            return Base64.getEncoder().withoutPadding()
                    .encodeToString(bytes);
        return null;
    }

    /***
     * Strips all non-digit values from a String
     *
     * This helps maintain a standard format for further decomposition
     */
    public static String stripIntoDigits ( String input ) {
        return input.replaceAll( "[^0-9]", "" );
    }

    public static String getFmSecureHash(String data, String secret) throws Exception {

        Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
        SecretKeySpec secret_key = new SecretKeySpec(Hex.decodeHex(secret.toCharArray()), "HmacSHA256");
        sha256_HMAC.init(secret_key);

        return new String(Hex.encodeHex(sha256_HMAC.doFinal(data.getBytes(StandardCharsets.UTF_8)))).toUpperCase();
    }
}
