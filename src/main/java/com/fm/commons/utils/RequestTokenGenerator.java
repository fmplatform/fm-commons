package com.fm.commons.utils;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class RequestTokenGenerator {


    public static String getToken(String requestId,String sourceCode,String password)
            throws NoSuchAlgorithmException, UnsupportedEncodingException {

        StringBuilder tokenSourceString = new StringBuilder();
        tokenSourceString.append(requestId);
        tokenSourceString.append(sourceCode);
        tokenSourceString.append(password);

        MessageDigest digest = MessageDigest.getInstance("SHA-512");
        // ** NOTE all bytes that are retrieved from the data string must be
        // done so using UTF-8 Character Set.
        byte[] hashBytes = tokenSourceString.toString().getBytes("UTF-8");
        //Create the hash bytes from the data
        byte[] messageDigest = digest.digest(hashBytes);
        //Create a HEX string from the hashed data
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < messageDigest.length; i++)
        {
            String h = Integer.toHexString(0xFF & messageDigest[i]);
            while(h.length() < 2)
                h = "0" + h;
            sb.append(h);
        }
        return sb.toString();
    }
}
