package com.fm.commons.interfaces;


import org.springframework.security.access.prepost.PreAuthorize;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@PreAuthorize("hasRole('CUSTOMER_SERVICE') or hasRole('FUND_MANAGER') or hasRole('ADMIN') or hasRole('COUNTRY_ADMIN') or hasRole('SYS_ADMIN')")
public @interface IsCustomerServiceOrAbove {
}
