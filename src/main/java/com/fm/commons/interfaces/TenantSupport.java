package com.fm.commons.interfaces;

public interface TenantSupport {
    String getTenantId();
    void setTenantId(String tenantId);
}
