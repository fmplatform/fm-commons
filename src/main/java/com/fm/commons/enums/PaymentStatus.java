package com.fm.commons.enums;

public enum PaymentStatus {

    PY_REQUESTED("REQ", "Requested", true),
    PY_PENDING("PEN", "Pending", true),
    PY_COMPLETED("CMP", "Completed", true),
    PY_UNKNOWN_TRANSACTION_ERROR("UTE", "Unknown transaction error", true),
    PY_INTERNAL_SERVER_ERROR("ISE", "Internal Server Error", true),
    API_PROCESSING_ERROR("ERROR", "Processing Error", false);


    private final Object code;
    private final String message;
    private final boolean house;

    PaymentStatus(final Object code, final String message, final boolean house) {
        this.code = code;
        this.message = message;
        this.house = house;
    }

    public Object getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public boolean isHouse() {
        return house;
    }

    public static PaymentStatus getByStatusCode(final Object statusCode) {
        PaymentStatus result = null;
        for (PaymentStatus status : values()) {
            if (status.getCode().equals(statusCode)) {
                result = status;
                break;
            }
        }
        return result;
    }

}
