package com.fm.commons.enums;

public enum TransactionType {
    MF_SUBSCRIPTION,
    SECURITY_PURCHASE;


    public static TransactionType getEnum(String str) {
        for (TransactionType transactionType : TransactionType.values()) {
            if (transactionType.name().equalsIgnoreCase(str))
                return transactionType;
        }
        return null;
    }

}
