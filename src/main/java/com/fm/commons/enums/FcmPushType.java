package com.fm.commons.enums;

public enum FcmPushType {

    ACTIVITY("actionable_activity"),
    TOPIC("topic"),
    ACTIONABLE_ACTIVITY("actionable_activity"),
    ACTIONABLE_TOPIC("actionable_topic"),
    USER_TARGETED("user_targeted"),
    ANNOUNCEMENT("announcement"),
    TOPIC_SUBSCRIPTION("topic");

    private final String type;

    FcmPushType(final String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public static FcmPushType getType(final String type) {
        FcmPushType result = null;
        for (FcmPushType fcmPushType : values()) {
            if (fcmPushType.getType().equalsIgnoreCase(type) ) {
                result = fcmPushType;
                break;
            }
        }
        return result;
    }
}
