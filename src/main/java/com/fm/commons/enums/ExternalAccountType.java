package com.fm.commons.enums;

public enum ExternalAccountType {
    EDC,
    ECOBANK,
    NEW;
}
