package com.fm.commons.enums;

public enum Role {

    SYS_ADMIN(100, "SYS_ADMIN", "System Administrator"),
    USER(101, "USER", "System User"),
    CUSTOMER_SERVICE(102, "CUSTOMER_SERVICE", "Customer Service"),
    FUND_MANAGER(103, "FUND_MANAGER", "Fund Manager"),
    COUNTRY_ADMIN(104, "COUNTRY_ADMIN", "Country Admin"),
    ADMIN(105, "ADMIN", "Administrator");

    private final int code;
    private final String name;
    private final String description;

    Role(final int code, final String name, final String description) {
        this.code = code;
        this.name = name;
        this.description = description;
    }

    public int getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public static Role getByCode(final int code) {
        Role result = null;
        for (Role role : values()) {
            if (role.getCode() == code) {
                result = role;
                break;
            }
        }
        return result;
    }

}
