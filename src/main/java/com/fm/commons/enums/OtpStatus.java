package com.fm.commons.enums;

public enum OtpStatus {

    PENDING("Pending"),
    USED("Used"),
    EXPIRED("Expired");

    private final String status;

    OtpStatus(final String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public static OtpStatus getByStatus(final String status) {
        OtpStatus result = null;
        for (OtpStatus otpStatus : values()) {
            if (otpStatus.getStatus().equals(status)) {
                result = otpStatus;
                break;
            }
        }
        return result;
    }
}
