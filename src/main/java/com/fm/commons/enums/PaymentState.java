package com.fm.commons.enums;

public enum PaymentState {

    OPEN,
    PENDING,
    CLOSED_CANCELED,
    CLOSED_FAILED,
    CLOSED_COMPLETED,
    UNKNOWN;

}
