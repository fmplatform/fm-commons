package com.fm.commons.enums;

public enum ActivityResourceType {

    DEVICE_CANDIDATE("DeviceCandidate"),
    FINANCIAL_PLAN("FinancialPlan"),
    FINANCIAL_CONTRIBUTION("FinancialContribution"),
    KYC("Kyc"),
    ACCOUNT_LINK("LinkAccount"),
    ONBOARDING_STATUS("OnboardingStatus");

    private final String type;

    ActivityResourceType(final String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public static ActivityResourceType getByType(final String type) {
        ActivityResourceType result = null;
        for (ActivityResourceType resourceType : values()) {
            if (resourceType.getType().equalsIgnoreCase(type)) {
                result = resourceType;
                break;
            }
        }
        return result;
    }
}
