package com.fm.commons.enums;

public enum OtpPurpose {

    SIGN_UP("Sign Up"),
    LOGIN("Login"),
    CONFIRM_PHONE("Confirm Phone"),
    CONFIRM_PHONE_BY_KEY("Confirm Phone By Key"),
    CONFIRM_EMAIL("Confirm Email"),
    RESET_PASSCODE("Reset Passcode");

    private final String name;

    OtpPurpose(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static OtpPurpose getByName(final String name) {
        OtpPurpose result = null;
        for (OtpPurpose otpStatus : values()) {
            if (otpStatus.getName().equals(name)) {
                result = otpStatus;
                break;
            }
        }
        return result;
    }
}
