package com.fm.commons.enums;

public enum AuthExceptionE {

    INVALID_FORMAT_FOR_LOGIN_CREDENTIALS(1100, "Invalid format for login credentials"),
    INVALID_LOGIN_CREDENTIALS(1101, "Invalid login credentials"),
    THIS_USER_IS_LOGGED_IN_ON_ANOTHER_DEVICE(1102, "This user is logged in on another device."),
    PHONE_NUMBER_NOT_ONBOARDED(1103, "Phone number not onboarded"),
    INVALID_COUNTRY_CODE_OR_PHONE_NUMBER_FORMAT(1104, "Invalid country code or phone number format"),
    CANNOT_ASSIGN_USER_TO_DEFAULT_USER_ROLE(1105, "Cannot assign user to default USER role"),
    SERVICE_NOT_AVAILABLE_IN_COUNTRY_WITH_CODE(1106, "Service not available in country with code"),
    USER_DOES_NOT_EXIST(1107, "User does not exist"),
    TOKEN_EXPIRED(1108, "Token expired"),
    INVALID_TOKEN(1109, "Invalid token"),
    INVALID_OTP(1109, "Invalid OTP"),
    INVALID_FORMAT_FOR_USER_PASSWORD(1110, "Invalid format for user password"),
    ACCOUNT_ALREADY_CLAIMED(1111, "This user account has already been claimed/activated"),
    ACCOUNT_ALREADY_ONBOARDED(1112, "User already onboarded");


    private final int code;
    private final String message;

    AuthExceptionE(final int code, final String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public static AuthExceptionE getByCode(final int code) {
        AuthExceptionE result = null;
        for (AuthExceptionE roleE : values()) {
            if (roleE.getCode() == code) {
                result = roleE;
                break;
            }
        }
        return result;
    }

}
