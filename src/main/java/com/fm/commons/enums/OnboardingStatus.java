package com.fm.commons.enums;

public enum OnboardingStatus {

    PENDING_KYC("PENDING_KYC"),
    PENDING_APPROVAL("PENDING_APPROVAL"),
    APPROVED("APPROVED"),
    BI_USER("BI_USER"),;

    private final String status;

    OnboardingStatus(final String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public static OnboardingStatus getByStatus(final String status) {
        OnboardingStatus result = null;
        for (OnboardingStatus otpStatus : values()) {
            if (otpStatus.getStatus().equals(status)) {
                result = otpStatus;
                break;
            }
        }
        return result;
    }
}
