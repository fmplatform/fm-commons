package com.fm.commons.enums;

public enum ActivityAction {

    PRIMARY_DEVICE_ELECTION("primary_device_election"),
    FINANCIAL_CONTRIBUTION_REMINDER("financial_contribution_reminder"),
    FINANCIAL_PLAN_CREATION("financial_plan_creation"),
    ONBOARDING_STATUS("onboarding_status"),
    KYC_STATUS("kyc_status"),
    ACCOUNT_LINK("LinkAccount");

    private final String action;

    ActivityAction(final String action) {
        this.action = action;
    }

    public String getAction() {
        return action;
    }

    public static ActivityAction getByAction(final String action) {

        ActivityAction result = null;
        for (ActivityAction activityAction : values()) {
            if (activityAction.getAction().equalsIgnoreCase(action) ) {
                result = activityAction;
                break;
            }
        }
        return result;
    }

}
