package com.fm.commons.enums;

public enum AccountStatus {

    ACTIVE,
    INACTIVE,
    DORMANT,
    SUSPENDED,
    BLOCKED;

}
