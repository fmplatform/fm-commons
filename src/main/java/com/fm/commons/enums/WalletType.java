package com.fm.commons.enums;

public enum WalletType {

    MOMO,
    BANK_CARD,
    ECOBANK,
    OTHER_BANK
}
