//package com.fm.commons.dtos;
//
//
//import org.junit.Assert;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import pl.pojo.tester.api.assertion.Method;
//
//import static pl.pojo.tester.api.assertion.Assertions.assertPojoMethodsFor;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@SpringBootTest
////@Ignore
//public class FmDtosTest {
//
//    @Test
//    public void testAccountPortfolioDto(){
//        final Class<?> classUnderTest = AccountPortfolioDto.class;
//        assertPojoMethodsFor(classUnderTest).areWellImplemented();
//    }
//
//    @Test
//    public void testUserGroupByCountryDto(){
//        final Class<?> classUnderTest = UsersGroupByCountry.class;
//        assertPojoMethodsFor(classUnderTest).areWellImplemented();
//    }
//
//    @Test
//    public void testCreateUserDto(){
//        final Class<?> classUnderTest = CreateUserDto.class;
//        assertPojoMethodsFor(classUnderTest).areWellImplemented();
//    }
//
//    @Test
//    public void testCreateWalletDto(){
//        final Class<?> classUnderTest = CreateWalletDto.class;
//        assertPojoMethodsFor(classUnderTest).areWellImplemented();
//    }
//
//    @Test
//    public void testPaymentDto(){
//        final Class<?> classUnderTest = PaymentDto.class;
//        assertPojoMethodsFor(classUnderTest).areWellImplemented();
//    }
//
//    @Test
//    public void testPaymentMethodDto(){
//        final Class<?> classUnderTest = PaymentMethodDto.class;
//        assertPojoMethodsFor(classUnderTest).areWellImplemented();
//    }
//
////    @Test
////    public void testPagedContent(){
////        final Class<?> classUnderTest = PagedContent.class;
////        assertPojoMethodsFor(classUnderTest).areWellImplemented();
////    }
//
//    @Test
//    public void testCurrencyDto(){
//        final Class<?> classUnderTest = CurrencyDto.class;
//        assertPojoMethodsFor(classUnderTest).areWellImplemented();
//    }
//
//    @Test
//    public void testCustomerAccountDto(){
//        final Class<?> classUnderTest = CustomerAccountDto.class;
//        assertPojoMethodsFor(classUnderTest).areWellImplemented();
//    }
//
//    @Test
//    public void testDeviceCandidateDto(){
//        final Class<?> classUnderTest = DeviceCandidateDto.class;
//        assertPojoMethodsFor(classUnderTest).areWellImplemented();
//
//        DeviceCandidateDto candidateDto = new DeviceCandidateDto();
//        candidateDto.setBrand("brancd");
//        Assert.assertNotNull(candidateDto.toActivityData());
//    }
//
//    @Test
//    public void testExternalAccountDto(){
//        final Class<?> classUnderTest = ExternalAccountDto.class;
//        assertPojoMethodsFor(classUnderTest).areWellImplemented();
//    }
//
//    @Test
//    public void testFindByCodeDto(){
//        final Class<?> classUnderTest = FindByCodeDto.class;
//        assertPojoMethodsFor(classUnderTest).areWellImplemented();
//    }
//
//    @Test
//    public void testFindByIdDto(){
//        final Class<?> classUnderTest = FindByIdDto.class;
//        assertPojoMethodsFor(classUnderTest).areWellImplemented();
//    }
//
//    @Test
//    public void testEditUserRoleDto(){
//        final Class<?> classUnderTest = EditUserRoleDto.class;
//        assertPojoMethodsFor(classUnderTest).areWellImplemented();
//    }
//
//    @Test
//    public void testFindByPhoneDto(){
//        final Class<?> classUnderTest = FindByPhoneDto.class;
//        assertPojoMethodsFor(classUnderTest).areWellImplemented();
//    }
//
//    @Test
//    public void testFindByStringCodeDto(){
//        final Class<?> classUnderTest = FindByStringCodeDto.class;
//        assertPojoMethodsFor(classUnderTest).areWellImplemented();
//    }
//
//    @Test
//    public void testFindByToken(){
//        final Class<?> classUnderTest = FindByTokenDto.class;
//        assertPojoMethodsFor(classUnderTest).areWellImplemented();
//    }
//
//    @Test
//    public void testSignUpDto(){
//        final Class<?> classUnderTest = SignupDto.class;
//        assertPojoMethodsFor(classUnderTest).areWellImplemented();
//    }
//
//    @Test
//    public void testSmsPayloadDto(){
//        final Class<?> classUnderTest = SmsPayloadDto.class;
//        assertPojoMethodsFor(classUnderTest).areWellImplemented();
//    }
//
//    @Test
//    public void testEmailPayloadDto(){
//        final Class<?> classUnderTest = EmailPayloadDto.class;
//        assertPojoMethodsFor(classUnderTest).areWellImplemented();
//    }
//
//    @Test
//    public void testFcmPayloadDto(){
//        final Class<?> classUnderTest = FcmPayloadDto.class;
//        assertPojoMethodsFor(classUnderTest).testing(Method.GETTER, Method.SETTER)
//                .testing(Method.EQUALS)
//                .testing(Method.HASH_CODE)
//                .testing(Method.CONSTRUCTOR)
//                .areWellImplemented();
//        FcmPayloadDto fcmPayloadDto = new FcmPayloadDto();
//        fcmPayloadDto.setData("{}");
//        Assert.assertNotNull(fcmPayloadDto.toString());
//    }
//
//    @Test
//    public void testSetOnboardingStatusDto(){
//        final Class<?> classUnderTest = SetOnboardingStatusDto.class;
//        assertPojoMethodsFor(classUnderTest).areWellImplemented();
//    }
//
//    @Test
//    public void testSendOtpDto(){
//        final Class<?> classUnderTest = SendOtpDto.class;
//        assertPojoMethodsFor(classUnderTest).areWellImplemented();
//    }
//
//    @Test
//    public void testRoleDto(){
//        final Class<?> classUnderTest = RoleDto.class;
//        assertPojoMethodsFor(classUnderTest).areWellImplemented();
//    }
//
//    @Test
//    public void testPrivacyPolicyDto(){
//        final Class<?> classUnderTest = PrivacyPolicyDto.class;
//        assertPojoMethodsFor(classUnderTest).areWellImplemented();
//    }
//
//    @Test
//    public void testResetPasswordDto(){
//        final Class<?> classUnderTest = ResetPassCodeDto.class;
//        assertPojoMethodsFor(classUnderTest).areWellImplemented();
//    }
//
//    @Test
//    public void testOtpDto(){
//        final Class<?> classUnderTest = OtpDto.class;
//        assertPojoMethodsFor(classUnderTest).areWellImplemented();
//    }
//
//    @Test
//    public void testLegalDto(){
//        final Class<?> classUnderTest = LegalDto.class;
//        assertPojoMethodsFor(classUnderTest).areWellImplemented();
//    }
//
//    @Test
//    public void testLoginDto(){
//        final Class<?> classUnderTest = LoginDto.class;
//        assertPojoMethodsFor(classUnderTest).areWellImplemented();
//    }
//
//    @Test
//    public void testKycDto(){
//        final Class<?> classUnderTest = KycDto.class;
//        assertPojoMethodsFor(classUnderTest).areWellImplemented();
//    }
//
//    @Test
//    public void testSetPasswordDto(){
//        final Class<?> classUnderTest = SetPasswordDto.class;
//        assertPojoMethodsFor(classUnderTest).areWellImplemented();
//    }
//
//    @Test
//    public void testSetPassCodeDto(){
//        final Class<?> classUnderTest = SetPassCodeDto.class;
//        assertPojoMethodsFor(classUnderTest).areWellImplemented();
//    }
//
//    @Test
//    public void testTermsOfServiceDto(){
//        final Class<?> classUnderTest = TermsOfServiceDto.class;
//        assertPojoMethodsFor(classUnderTest).areWellImplemented();
//    }
//
//    @Test
//    public void testDeviceDto(){
//        final Class<?> classUnderTest = DeviceDto.class;
//        assertPojoMethodsFor(classUnderTest).areWellImplemented();
//    }
//
//    @Test
//    public void testUserGroupByType(){
//        final Class<?> classUnderTest = UsersGroupByTypeDto.class;
//        assertPojoMethodsFor(classUnderTest).areWellImplemented();
//    }
//
//    @Test
//    public void testUserCreateDeviceCandidateDto(){
//        final Class<?> classUnderTest = CreateDeviceCandidateDto.class;
//        assertPojoMethodsFor(classUnderTest).areWellImplemented();
//    }
//
//    @Test
//    public void testUserCreateEDCAccountDto(){
//        final Class<?> classUnderTest = CreateEDCAccountDto.class;
//        assertPojoMethodsFor(classUnderTest).areWellImplemented();
//    }
//
//    @Test
//    public void testAccountValidationTokenDto(){
//        // given
//        final Class<?> classUnderTest = AccountValidationTokenDto.class;
//
//        // then
//        assertPojoMethodsFor(classUnderTest).areWellImplemented();
//    }
//
//    @Test
//    public void testApiResponse(){
//        // given
//        final Class<?> classUnderTest = ApiResponse.class;
//
//        // then
//        assertPojoMethodsFor(classUnderTest).areWellImplemented();
//    }
//
//    @Test
//    public void testBiLoginDto() {
//        // given
//        final Class<?> classUnderTest = BiLoginDto.class;
//
//        // then
//        assertPojoMethodsFor(classUnderTest).areWellImplemented();
//    }
//
//    @Test
//    public void testCountryDto(){
//        // given
//        final Class<?> classUnderTest = CountryDto.class;
//
//        // then
//        assertPojoMethodsFor(classUnderTest).areWellImplemented();
//    }
//
//    @Test
//    public void testWalletDto(){
//        // given
//        final Class<?> classUnderTest = WalletDto.class;
//
//        // then
//        assertPojoMethodsFor(classUnderTest).areWellImplemented();
//        WalletDto walletDto = new WalletDto();
//        Assert.assertNotNull(walletDto.toActivityData());
//    }
//
////    @Test
////    public void testUserDto(){
////        // given
////        final Class<?> classUnderTest = UserDto.class;
////
////        // then
////        assertPojoMethodsFor(classUnderTest).areWellImplemented();
////        UserDto userDto = new UserDto();
////        userDto.setFirstName("Foo");
////        userDto.setLastName("Bar");
////        Assert.assertEquals(userDto.getFullName(), "Foo Bar");
////    }
//
//    @Test
//    public void testVerifyOtpDto(){
//        // given
//        final Class<?> classUnderTest = VerifyOtpDto.class;
//
//        // then
//        assertPojoMethodsFor(classUnderTest).areWellImplemented();
//    }
//
//    @Test
//    public void testActivityDto(){
//        // given
//        final Class<?> classUnderTest = ActivityDto.class;
//
//        assertPojoMethodsFor(classUnderTest).testing(Method.GETTER, Method.SETTER)
//                .testing(Method.EQUALS)
//                .testing(Method.HASH_CODE)
//                .testing(Method.CONSTRUCTOR)
//                .areWellImplemented();
//        ActivityDto activityDto = new ActivityDto();
//        Assert.assertNotNull(activityDto.toString());
//    }
//
//    @Test
//    public void testFundQuotationDto(){
//        // given
//        final Class<?> classUnderTest = FundQuotationDto.class;
//
//        // then
//        assertPojoMethodsFor(classUnderTest).areWellImplemented();
//    }
//
////    @Test
////    public void testMutualFundDto(){
////        final Class<?> classUnderTest = MutualFundDto.class;
////        assertPojoMethodsFor(classUnderTest).testing(Method.GETTER, Method.SETTER)
////                .testing(Method.EQUALS)
////                .testing(Method.HASH_CODE)
////                .testing(Method.CONSTRUCTOR)
////                .areWellImplemented();
////    }
//}
